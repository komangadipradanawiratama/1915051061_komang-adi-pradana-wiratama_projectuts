import 'package:flutter/material.dart';
import 'data.dart';
import 'detail.dart';
import 'home.dart';
import 'login_page.dart';


void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    Home.tag: (context) => Home(),
  };

  @override

  Widget build(BuildContext context) {
    return MaterialApp(
         home: LoginPage(),
      routes: routes,
    );
  }
}

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Book.Shop',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
        platform: TargetPlatform.iOS,
      ),
      home: Home(),
      onGenerateRoute: (settings) => generateRoute(settings),
    );
  }

  generateRoute(RouteSettings settings) {
    final path = settings.name.split('/');
    final title = path[1];

    Book book = books.firstWhere((it) => it.title == title);
    return MaterialPageRoute(
      settings: settings,
      builder: (context) => Detail(book),
    );
  }

