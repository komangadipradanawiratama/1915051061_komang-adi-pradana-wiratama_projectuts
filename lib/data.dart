class Book {
  String title,
      writer,
      price,
      image,
      description =
          'Komik ini mempunyai rating yang sanagat bagus, dengan alur cerita yang sanagat menegangkan sehingga cocok untuk anda yang ingin pengalaman membaca komik yang penuh tantangan. \nAyo segera dapatkan komik ini hanya di BOOK.SHOP dan jangan sampai kehabisan karena stock terbatas !!.';
  int pages;
  double rating;

  Book(
      this.title, this.writer, this.price, this.image, this.rating, this.pages);
}

final List<Book> books = [
  Book('Komik Black Clover ', 'Yuki Tabata',
      'Rp 50.000', 'Assets/komik_C.jpg', 3.5, 123),
  Book('Komik Detective Conan', 'Gosho Aoyama', 'Rp 55.000',
      'Assets/komik_conan.jpg', 4.5, 200),
  Book('Komik Naruto', 'Masashi Kishimoto',
      'Rp 60.000', 'Assets/komik_narto.jpg', 5.0, 324),
  Book('Komik Boruto', 'Masashi Kisimoto', 'Rp 58.000',
      'Assets/komik_borto.jpg', 3.0, 200),
  Book('Komik One Piece', 'Eiichiro Oda',
      'Rp 90.000', 'Assets/komik_nepiece.jpg', 4.8, 234),
  Book('Komik Doraemon', 'Fujiko Fujio',
      'Rp 57.000', 'Assets/komik_doraemon.jpg', 4.5, 240),
  Book('Komik Jujutsu No Kaisen', 'Gege Akutami',
      'Rp 56.000', 'Assets/komik_jjk.jpg', 4.8, 432),
  Book('Komik Boku No Hero Academia', 'Kohei Horikoshi',
      'Rp 56.000', 'Assets/komik_hero.jpg', 4.8, 432),
  Book('Komik Tokyo Ghouls', 'Sui Ishida',
      'Rp 55.000', 'Assets/komik_tkg.jpg', 4.5, 321),

 ];